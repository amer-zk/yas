let slideIndex = 1;

/* auto play logic */
const slideInterval = setInterval(() => {
  plusSlides(1);
}, 10000);
function stopSlideInterval() {
  clearInterval(slideInterval);
}
/* auto play logic end */

showSlides(slideIndex);

function plusSlides(n) {
  showSlides((slideIndex += n));
}

function currentSlide(n) {
  showSlides((slideIndex = n));
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
}

/* handlers */
function handleNextButton() {
  plusSlides(1);
  stopSlideInterval();
}

function handlePrevButton() {
  plusSlides(-1);
  stopSlideInterval();
}

function handleDotButton(n) {
  currentSlide(n);
  stopSlideInterval();
}
